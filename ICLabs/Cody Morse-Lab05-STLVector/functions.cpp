#include <iostream>         // Use input and output streams
#include <string>           // Using string (for text)
#include <iomanip>          // Special library for formatting text (used in my tests)
#include <vector>           // Use the vector class
using namespace std;        // Use the STandarD library

#include "functions.hpp"    // Function headers are stored here

// USER-DEFINED FUNCTIONS - You will edit these!

/***********************************************/
/** Function Set 1 ****************************/

//! Add the following items to the classList using .push_back(): cs134, cs200, cs235, cs250, cs210, and cs211.
void GetClassList(vector<string>& classList)
{
	classList.push_back("cs134");
	classList.push_back("cs200");
	classList.push_back("cs235");
	classList.push_back("cs250");
	classList.push_back("cs210");
	classList.push_back("cs211");
}

//! Remove the last ("back") item from the classList passed in using .pop_back()
void RemoveLastItem(vector<string>& classList)
{
	classList.pop_back();
}

//! Clear all the items out of classList using .clear()
void ClearList(vector<string>& classList)
{
	classList.clear();
}

//! Return the first ("front") item from classList using .front()
string GetFirst(vector<string>& classList)
{
	return classList.front();
}

//! Return the last ("back") item from classList using .back()
string GetLast(vector<string>& classList)
{
	return classList.back();
}

//! Get a specific item from the list at some index, using [] or .at()
string GetItem(vector<string>& classList, int index)
{
	return classList[index];
}

//! Build a string with all the class names, separated by whitespace.
string GetAllItems(const vector<string>& classList)
{
	string items = "";
	for (int i = 0; i < classList.size(); i++)
	{

		items += classList[i] + "  ";
	}
	return items;
}

/** Program code **/
void Program()
{
    vector<string> classList;

    bool quit = false;
    while ( quit == false )
    {
        ClearScreen();
        cout << "************************************" << endl;
        cout << "**             Vector             **" << endl;
        cout << "************************************" << endl;
        cout << " 1. Set prerequisites" << endl;
        cout << " 2. Remove last item" << endl;
        cout << " 3. Clear the list" << endl;
        cout << " 4. Get first prerequisite" << endl;
        cout << " 5. Get last prerequisite" << endl;
        cout << " 6. Look at item" << endl;
        cout << " 7. Display all items" << endl;
        cout << " 8. Quit" << endl << endl;

        int choice;
        cout << "Run which function? ";
        cin >> choice;

        cout << endl << endl;

        switch( choice )
        {
            case 1:
                GetClassList( classList );
                cout << "Added classes to list" << endl;
            break;

            case 2:
                RemoveLastItem( classList );
                cout << "Removed last item" << endl;
            break;

            case 3:
                ClearList( classList );
                cout << "Cleared list" << endl;
            break;

            case 4:
            {
                string first = GetFirst( classList );
                cout << "First item: " << first << endl;
                break;
            }

            case 5:
            {
                string last = GetLast( classList );
                cout << "Last item: " << last << endl;
                break;
            }

            case 6:
            {
                int index;
                cout << "Which index? " << endl;
                cin >> index;
                string item = GetItem( classList, index );
                cout << "Class: " << item << endl;
                break;
            }

            case 7:
            {
                string allItems = GetAllItems( classList );
                cout << allItems << endl;
                break;
            }

            case 8:
                quit = true;
            break;
        }

        Pause();

        cout << endl << endl;
    }
}

// Tester functions (DO NOT MODIFY) ----------------------------------------------------
void RunTests()
{
    Test_Set1();
    Test_Set2();
    Test_Set3();
    Test_Set4();
    Test_Set5();
    Test_Set6();
    Test_Set7();
}

void Test_Set1()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetClassList" << endl;
    vector<string> actualOut;
    string expectedOut[] = { "cs134", "cs200", "cs235", "cs250", "cs210", "cs211" };

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check amount of items returned" << setw( pfWidth );
    GetClassList( actualOut );

    unsigned int expectedSize = 6;
    unsigned int actualSize = actualOut.size();

    if ( actualSize != expectedSize )   { cout << " x FAIL\n\t EXPECTED: \"" << expectedSize << "\" \n\t ACTUAL:   \"" << actualSize << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 2: Check the values returned" << setw( pfWidth );

    bool allMatch = true;
    string actualValues = "{";
    for ( unsigned int i = 0; i < actualSize; i++ )
    {
        if ( i > 0 ) { actualValues += ", "; }
        actualValues += "\"" + actualOut[i] + "\"";
        if ( actualOut[i] != expectedOut[i] )
        {
            allMatch = false;
        }
    }
    if ( actualSize < expectedSize ) { allMatch = false; }
    actualValues += "}";

    if ( !allMatch )    { cout << " x FAIL\n\t EXPECTED: {\"cs134\", \"cs200\", \"cs235\"} \n\t ACTUAL:   " << actualValues << endl; }
    else                { cout << " * PASS" << endl; }
}

void Test_Set2()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - RemoveLastItem" << endl;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that the last item is removed" << setw( pfWidth );

    vector<string> actualOut = { "a", "b", "c" };
    vector<string> expectedOut = { "a", "b" };

    RemoveLastItem( actualOut );
    unsigned int actualSize = actualOut.size();
    unsigned int expectedSize = 2;

    bool allMatch = true;
    string actualValues = "{";
    for ( unsigned int i = 0; i < actualSize; i++ )
    {
        if ( i > 0 ) { actualValues += ", "; }
        actualValues += "\"" + actualOut[i] + "\"";
        if ( actualOut[i] != expectedOut[i] )
        {
            allMatch = false;
        }
    }
    if ( actualSize < expectedSize ) { allMatch = false; }
    actualValues += "}";

    if ( !allMatch )    { cout << " x FAIL\n\t EXPECTED: {\"a\", \"b\", \"c\"} \n\t ACTUAL:   " << actualValues << endl; }
    else                { cout << " * PASS" << endl; }

}

void Test_Set3()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - ClearList" << endl;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that size is correct" << setw( pfWidth );
    vector<string> vec = { "a", "b", "c" };
    ClearList( vec );

    int expectedOut = 0;
    int actualOut = vec.size();

    if ( actualOut != expectedOut )     { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }
}

void Test_Set4()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetFirst" << endl;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Make sure correct item is returned" << setw( pfWidth );
    vector<string> vec = { "a", "b", "c" };

    string expectedOut = "a";
    string actualOut = GetFirst( vec );

    if ( actualOut != expectedOut )     { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }
}

void Test_Set5()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetLast" << endl;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Make sure correct item is returned" << setw( pfWidth );
    vector<string> vec = { "a", "b", "c" };

    string expectedOut = "c";
    string actualOut = GetLast( vec );

    if ( actualOut != expectedOut )     { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }

}

void Test_Set6()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetItem" << endl;
    vector<string> vec = { "a", "b", "c" };

    cout << endl << left << setw( headerWidth ) << "TEST 1: Make sure correct item is returned" << setw( pfWidth );

    string expectedOut = "a";
    string actualOut = GetItem( vec, 0 );

    if ( actualOut != expectedOut )     { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 2: Make sure correct item is returned" << setw( pfWidth );

    expectedOut = "b";
    actualOut = GetItem( vec, 1 );

    if ( actualOut != expectedOut )     { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 3: Make sure correct item is returned" << setw( pfWidth );

    expectedOut = "c";
    actualOut = GetItem( vec, 2 );

    if ( actualOut != expectedOut )     { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                                { cout << " * PASS" << endl; }

}

void Test_Set7()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetAllItems" << endl;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that correct string is returned" << setw( pfWidth );

    vector<string> vec = { "a", "b", "c" };

    bool containsAll = true;
    string actualOut = GetAllItems( vec );
    if (    actualOut.find( "a" ) == string::npos ||
            actualOut.find( "b" ) == string::npos ||
            actualOut.find( "c" ) == string::npos )
    {
        containsAll = false;
    }

    if ( !containsAll )         { cout << " x FAIL\n\t EXPECTED: \"a b c\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
    else                        { cout << " * PASS" << endl; }
}

string B2S( bool val )
{
    return ( val ) ? "true" : "false";
}

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    system( "cls" );
    #else
    system( "clear" );
    #endif
}

void Pause()
{
    cout << "Press enter to continue..." << endl;
    cin.ignore();
    cin.get();
}


