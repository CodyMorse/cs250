#include <iostream>         // Use input and output streams
#include <string>           // Using string (for text)
#include <iomanip>          // Special library for formatting text (used in my tests)
using namespace std;        // Use the STandarD library

#include "functions.hpp"    // Function headers are stored here

// USER-DEFINED FUNCTIONS - You will edit these!

/***********************************************/
/** Function Set 1 ****************************/

string Alphabet_Iter( char start, char end )
{ 
	string word = "";
	for (char letter = start; letter <= end; letter++)
	{
		word += letter;
	}
    return word; 
}

string Alphabet_Rec( char start, char end, string text /* = "" */ )
{
	if (start > end)
	{
		return text;
	}

	text += start;
	return Alphabet_Rec(start + 1, end, text);
}
/***********************************************/
/** Function Set 2 ****************************/

int Factorial_Iter( int n )
{
	if (n == 0)
	{
		return 1;
	}
	for (int i = n -1 ; i > 0 ; i--)
	{
		n *= i;
	}
	return n;
}

int Factorial_Rec( int n )
{
	if (n == 0)
	{
		return 1;
	}

	if (n > 0)
	{
		return n * Factorial_Rec(n - 1);
	}
}

/***********************************************/
/** Function Set 3 ****************************/

//! Helper function to find whether something is a consonant or not.
bool IsConsonant( char letter )
{
    if (    tolower( letter ) == 'a' ||
            tolower( letter ) == 'e' ||
            tolower( letter ) == 'i' ||
            tolower( letter ) == 'o' ||
            tolower( letter ) == 'u'
        )
    {
        return false;
    }
	else
	{
		return true;
	}
}

int CountConsonants_Iter( string text )
{
	int totalCons = 0;
	for (unsigned int i = 0; i < text.size(); i++)
	{
		if (IsConsonant(text[i]) == true)
		{
			totalCons++;
		}

		cout << endl << "Letter " << i << " is " << text[i] << endl;	
	}
	return totalCons;
}

int CountConsonants_Rec( string text, unsigned int pos /* = 0 */ )
{
	if (pos == text.size())
	{
		return 0;
	}
	if (IsConsonant(text[pos]) == true)
	{
		return 1 + CountConsonants_Rec(text, pos + 1);
	}	
		return 0 + CountConsonants_Rec(text, pos + 1);
}

/***********************************************/
/** Function Set 4 ****************************/

//! Helper function to figure out if letter is upper-case
bool IsUppercase( char letter )
{
    return ( letter != ' ' && toupper( letter ) == letter );
}

char GetFirstUppercase_Iter( string text )
{
	for (unsigned int pos = 0; pos < text.size(); pos++)
	{
		if (IsUppercase(text[pos]))
		{
			return text[pos];
		}
	}
    return ' '; 
}

char GetFirstUppercase_Rec( string text, unsigned int pos /* = 0 */ )
{
	if (pos == text.size())
	{
		return ' ';
	}
	if (IsUppercase(text[pos]))
	{
		return text[pos];
	}
	return GetFirstUppercase_Rec(text, pos + 1);
}


/** Program code **/
void Program()
{
    bool quit = false;
    while ( quit == false )
    {
        ClearScreen();
        cout << "***************************************" << endl;
        cout << "**             RECURSION             **" << endl;
        cout << "***************************************" << endl;
        cout << " 1. Alphabet" << endl;
        cout << " 2. Factorial" << endl;
        cout << " 3. GetConsonants" << endl;
        cout << " 4. GetFirstUppercase" << endl;
        cout << " 5. Quit" << endl << endl;

        int choice;
        cout << "Run which function? ";
        cin >> choice;

        cout << endl << endl;

        switch( choice )
        {
            case 1:
            {
                char start;
                char end;

                cout << "Enter starting letter and ending letter: ";
                cin >> start >> end;
                cout << endl;

                cout << "Alphabet, Iterative:" << endl;
                string result = Alphabet_Iter( start, end );
                cout << result << endl;

                cout << endl << endl;
                cout << "Alphabet, Recursive:" << endl;
                result = Alphabet_Rec( start, end, "" );
                cout << result << endl;
            }
            break;

            case 2:
            {
                int n;
                cout << "Enter a value for n: ";
                cin >> n;
                cout << endl;

                cout << "Factorial, Iterative:" << endl;
                cout << n << "! = " << Factorial_Iter( n ) << endl;

                cout << endl << endl;
                cout << "Factorial, Recursive:" << endl;
                cout << n << "! = " << Factorial_Rec( n ) << endl;
            }
            break;

            case 3:
            {
                string text;
                cout << "Enter a word (no spaces): ";
                cin >> text;
                cout << endl;

                cout << "GetConsonants, Iterative:" << endl;
                cout << " * Consonants in " << text << ": " << CountConsonants_Iter( text ) << endl;

                cout << endl << endl;
                cout << "GetConsonants, Recursive:" << endl;
                cout << " * Consonants in " << text << ": " << CountConsonants_Rec( text ) << endl;
            }
            break;

            case 4:
            {
                string text[] = { "how are YOU?", "What?", "where am I?", "no caps" };

                cout << "GetFirstUppercase, Iterative:" << endl;
                for ( int i = 0; i < 4; i++ )
                {
                    cout << " * First upper-case in " << text[i] << ": '" << GetFirstUppercase_Iter( text[i] ) << "'" << endl;
                }

                cout << endl << endl;
                cout << "GetConsonants, Recursive:" << endl;
                for ( int i = 0; i < 4; i++ )
                {
                    cout << " * First upper-case in " << text[i] << ": '" << GetFirstUppercase_Rec( text[i], 0 ) << "'" << endl;
                }
            }
            break;

            case 5:
                quit = true;
            break;
        }

        cout << endl << endl;
    }
}

// Tester functions (DO NOT MODIFY) ----------------------------------------------------
void RunTests()
{
    Test_Set1();
    Test_Set2();
    Test_Set3();
    Test_Set4();
}

string B2S( bool val )
{
    return ( val ) ? "true" : "false";
}

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    system( "cls" );
    #else
    system( "clear" );
    #endif
}

void Pause()
{
    cout << "Press enter to continue..." << endl;
    cin.ignore();
    cin.get();
}

void Test_Set1()
{
    //string Alphabet_Iter( char start, char end )
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - Alphabet" << endl;
    string expectedOut, actualOut;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Alphabet_Iter: Generate 'a' thru 'g'" << setw( pfWidth );
    expectedOut = "abcdefg";
    actualOut = Alphabet_Iter( 'a', 'g' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 2: Alphabet_Iter: Generate 'l' thru 'p'" << setw( pfWidth );
    expectedOut = "lmnop";
    actualOut = Alphabet_Iter( 'l', 'p' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 3: Alphabet_Rec: Generate 'a' thru 'g'" << setw( pfWidth );
    expectedOut = "abcdefg";
    actualOut = Alphabet_Rec( 'a', 'g' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 4: Alphabet_Rec: Generate 'l' thru 'p'" << setw( pfWidth );
    expectedOut = "lmnop";
    actualOut = Alphabet_Rec( 'l', 'p' );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

}

void Test_Set2()
{
    // int Factorial_Iter( int n );
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - Factorial" << endl;
    int expectedOut, actualOut;

    cout << endl << left << setw( headerWidth ) << "TEST 1: Factorial_Iter: Find 0!" << setw( pfWidth );
    expectedOut = 1;
    actualOut = Factorial_Iter( 0 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 2: Factorial_Iter: Find 5!" << setw( pfWidth );
    expectedOut = 120;
    actualOut = Factorial_Iter( 5 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 3: Factorial_Rec: Find 0!" << setw( pfWidth );
    expectedOut = 1;
    actualOut = Factorial_Rec( 0 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 4: Factorial_Rec: Find 5!" << setw( pfWidth );
    expectedOut = 120;
    actualOut = Factorial_Rec( 5 );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }
}

void Test_Set3()
{
    // int CountConsonants_Iter( string text )
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - CountConsonants" << endl;
    int expectedOut, actualOut;

    cout << endl << left << setw( headerWidth ) << "TEST 1: CountConsonants_Iter: Count consonants in \"aeiou\"" << setw( pfWidth );
    expectedOut = 0;
    actualOut = CountConsonants_Iter( "aeiou" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 2: CountConsonants_Iter: Count consonants in \"jkl\"" << setw( pfWidth );
    expectedOut = 3;
    actualOut = CountConsonants_Iter( "jkl" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 3: CountConsonants_Iter: Count consonants in \"hellothere\"" << setw( pfWidth );
    expectedOut = 6;
    actualOut = CountConsonants_Iter( "hellothere" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }



    cout << endl << left << setw( headerWidth ) << "TEST 4: CountConsonants_Rec: Count consonants in \"aeiou\"" << setw( pfWidth );
    expectedOut = 0;
    actualOut = CountConsonants_Rec( "aeiou" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 5: CountConsonants_Rec: Count consonants in \"jkl\"" << setw( pfWidth );
    expectedOut = 3;
    actualOut = CountConsonants_Rec( "jkl" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

    cout << endl << left << setw( headerWidth ) << "TEST 6: CountConsonants_Rec: Count consonants in \"hellothere\"" << setw( pfWidth );
    expectedOut = 6;
    actualOut = CountConsonants_Rec( "hellothere" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

}

void Test_Set4()
{
    //char GetFirstUppercase_Iter( string text )
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetFirstUppercase" << endl;
    char expectedOut, actualOut;

    cout << endl << left << setw( headerWidth ) << "TEST 1: GetFirstUppercase_Iter: Find first consonant in \"HELLO\"" << setw( pfWidth );
    expectedOut = 'H';
    actualOut = GetFirstUppercase_Iter( "HELLO" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 2: GetFirstUppercase_Iter: Find first consonant in \"heLLO\"" << setw( pfWidth );
    expectedOut = 'L';
    actualOut = GetFirstUppercase_Iter( "heLLO" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 3: GetFirstUppercase_Iter: Find first consonant in \"hello\"" << setw( pfWidth );
    expectedOut = ' ';
    actualOut = GetFirstUppercase_Iter( "hello" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }



    cout << endl << left << setw( headerWidth ) << "TEST 4: GetFirstUppercase_Rec: Find first consonant in \"HELLO\"" << setw( pfWidth );
    expectedOut = 'H';
    actualOut = GetFirstUppercase_Rec( "HELLO" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 5: GetFirstUppercase_Rec: Find first consonant in \"heLLO\"" << setw( pfWidth );
    expectedOut = 'L';
    actualOut = GetFirstUppercase_Rec( "heLLO" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }


    cout << endl << left << setw( headerWidth ) << "TEST 6: GetFirstUppercase_Rec: Find first consonant in \"hello\"" << setw( pfWidth );
    expectedOut = ' ';
    actualOut = GetFirstUppercase_Rec( "hello" );

    if ( actualOut == expectedOut )     { cout << " * PASS" << endl; }
    else                                { cout << " x FAIL\n\t EXPECTED: \"" << expectedOut << "\" \n\t ACTUAL:   \"" << actualOut << "\"" << endl; }

}


