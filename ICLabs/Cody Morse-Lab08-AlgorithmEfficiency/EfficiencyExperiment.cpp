#include "EfficiencyExperiment.hpp"

#include <iostream>
using namespace std;

/*******************************/
/** IMPLEMENT THESE FUNCTIONS **/
/*******************************/

/**
This class contains a vector<int> m_array. Search this m_array.

@param      int     findMe          The # to find in the array
@param      bool    withOutput      Whether or not to display text as you're searching.
@return     int                     The index where the item was found, or -1 if not found.
*/
int EfficiencyExperiment::LinearSearch( int findMe, bool withOutput )
{
	for (int i = 0; i < m_array.size(); i++)
	{
		if (m_array[i] == findMe)
		{
			return i;
		}
	}
	return -1;
}

/**
This class contains a vector<int> m_array. Search this m_array.

@param      int     findMe          The # to find in the array
@param      bool    withOutput      Whether or not to display text as you're searching.
@return     int                     The index where the item was found, or -1 if not found.
*/
int EfficiencyExperiment::BinarySearch(int findMe, bool withOutput)
{
	int left = 0;
	int right = m_array.size() - 1;
	int mid;

	while (left <= right)
	{
		mid = left + (right - left) / 2;
		if (m_array[mid] == findMe)
		{
			return mid;
		}
		else if (m_array[mid] < findMe)
		{
			left = mid + 1;
		}
		else if (m_array[mid] > findMe)
		{
			right = mid - 1;

		}
	}
	return -1;
}

/**
This class contains a vector<int> m_array. Search this m_array.

Come up with your own search method and implement it here.

@param      int     findMe          The # to find in the array
@param      bool    withOutput      Whether or not to display text as you're searching.
@return     int                     The index where the item was found, or -1 if not found.
*/
int EfficiencyExperiment::StudentSearch( int findMe, bool withOutput )
{
	int randomNumber = 0;

	while (m_array[randomNumber] != findMe)
	{
		randomNumber = rand() % m_array.size();
		
		if (randomNumber == findMe)
		{
			return randomNumber;
		}
	}
	return -1;
}


/**
n:              1   2   3   4   5   6   7   8   9   10   ...
n-th term:      1   1   2   3   5   8   13  21  34  55  ...

The Fibonacci sequence at a[n] is going to be a[n-1] + a[n-2],
or, the value of the previous two numbers in the sequence summed together.

@param      int     n               The "position" of the number to generate.
@return     int                     The n-th value of the Fibonacci sequence
*/
int EfficiencyExperiment::Fibonacci_Rec(unsigned int n )
{
	if (n <= 0)
	{
		return 0;
	}
	if (n == 1)
	{
		return 1;
	}
	else
	{
		return Fibonacci_Rec(n - 1) + Fibonacci_Rec(n - 2);
	}
}

/**
@param      int     n               The "position" of the number to generate.
@return     int                     The n-th value of the Fibonacci sequence
*/
int EfficiencyExperiment::Fibonacci_Iter( unsigned int n )
{
	unsigned int next = 1, previous = 1, current = 1;

	for (int i = 3; i < n; i++)
	{
		next = current + previous;
		previous = current;
		current = next;
	}
	return next;
}

/*********************************/
/** DON'T EDIT THESE            **/
/*********************************/

void EfficiencyExperiment::SetupVector( int arraySize, bool sorted )
{
    if ( m_array.size() > 0 ) { m_array.clear(); }

    for ( int i = 0; i < arraySize; i++ )
    {
        if ( sorted )
        {
            m_array.push_back( i+5 );
            m_listSorted = true;
        }
        else
        {
            // Random #s
            m_array.push_back( rand() % ( arraySize * 10 + 1 ) );
            m_listSorted = false;
        }
    }
}

int EfficiencyExperiment::Size()
{
    return m_array.size();
}
