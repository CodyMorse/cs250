#ifndef _VECTOR_HPP
#define _VECTOR_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

class Vector
{
    private:
    /* Member Variables */
    //! A string pointer used to allocate memory for a dynamic array.
    string* m_data;

    //! Stores how many items have been added to the Vector; less than or equal to the m_arraySize.
    int m_itemCount;

    //! Stores how large the array currently is; update after Resize() is called.
    int m_arraySize;

    public:
    /* Member Functions */
    // Constructor and Destructor
    Vector();
    ~Vector();

    // Deal with end of vector
    void Push( const string& newItem );

    // Deal with middle of vector
    string Get( int index ) const;
    void Remove( int index );

    // Helper functions
    int Size() const;
    bool IsFull() const;
    bool IsEmpty() const;

    private:
    // Internal helper functions
    bool IsInvalidIndex( int index ) const;
    bool IsElementEmpty( int index );

    // Memory management
    void AllocateMemory( int newSize = 10 );
    void DeallocateMemory();
    void Resize();

    // Special function, since we haven't covered exceptions yet
    void Panic( string message ) const;
    void NotImplemented() const;

    friend class Tester;
};

/* ****************************************************************************/
/* ************************************************* INITIALIZATION FUNCTIONS */
/* ****************************************************************************/

Vector::Vector() 
{
	m_itemCount = 0;
	m_arraySize = 0;
	m_data = nullptr;
}

Vector::~Vector() 
{
	DeallocateMemory();
}

bool Vector::IsEmpty() const 
{

	return (m_itemCount == 0);
}

bool Vector::IsInvalidIndex( int index ) const 
{
	return (index < 0 || index >= m_arraySize);
}

bool Vector::IsFull() const                                                 
{
	return (m_itemCount == m_arraySize);
}

int Vector::Size() const 
{
	return m_itemCount;
}

bool Vector::IsElementEmpty( int index ) 
{
	if (IsInvalidIndex(index))
	{
		Panic("invalid index");
	}
	return (m_data[index] == "");
}

/* ****************************************************************************/
/* ********************************************** MEMORY MANAGEMENT FUNCTIONS */
/* ****************************************************************************/

void Vector::AllocateMemory( int newSize /* = 10 */ )                      
{
	if (m_data == nullptr)
	{
		m_arraySize = newSize;
		m_itemCount = 0;
		m_data = new string[m_arraySize];
	}
}

void Vector::DeallocateMemory()                                           
{
	if (m_data != nullptr)
	{
		delete[] m_data;
		m_data = nullptr;
		m_arraySize = 0;
		m_itemCount = 0;
	}
}

void Vector::Resize()                                                        
{
	string* newArray = new string[m_arraySize + 10];
	for (int i = 0; i < m_itemCount; i++)
	{
		newArray[i] = m_data[i];
	}
	delete[] m_data;
	m_data = newArray;
	m_arraySize = m_arraySize + 10;
}

/* ****************************************************************************/
/* *********************************************** END-OF-ARRAY FUNCTIONALITY */
/* ****************************************************************************/

void Vector::Push( const string& newItem )                                  
{
	if (m_data == nullptr)
	{
		AllocateMemory();
	}
	else if (IsFull())
	{
		Resize();
	}
	for (int i = 0; i < m_arraySize; i++)
	{
		if (m_data[i] == "")
		{
			m_data[i] = newItem;
			m_itemCount++;
			break;
		}
	}
}

/* ****************************************************************************/
/* ************************************************************** ANY ELEMENT */
/* ****************************************************************************/

string Vector::Get( int index ) const 
{
	if (IsInvalidIndex(index))
	{
		Panic("invalid index");
	}
	return m_data[index];
}

void Vector::Remove( int index )                                      
{
	if (IsInvalidIndex(index))
	{
		Panic("invalid index");
	}
	m_data[index] = "";
	m_itemCount--;
}

/* ****************************************************************************/
/* ************************************************* FUNCTION TO THROW ERRORS */
/* ****************************************************************************/

//! Call this function if something terrible goes wrong.
void Vector::Panic( string message ) const /*                                   Panic */
{
    throw runtime_error( message );
}

//! Marks when a function hasn't been implemented yet.
void Vector::NotImplemented() const /*                                          NotImplemented */
{
    throw runtime_error( "Function not implemented yet!" );
}

#endif
