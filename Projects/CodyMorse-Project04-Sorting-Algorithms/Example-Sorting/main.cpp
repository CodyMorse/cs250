#include <iostream>
#include <string>
using namespace std;

#include "SortFunctions/bubbleSort.hpp"
#include "SortFunctions/insertionSort.hpp"
#include "SortFunctions/mergesort.hpp"
#include "SortFunctions/quicksort.hpp"
#include "SortFunctions/selectionSort.hpp"
#include "SortFunctions/shellSort.hpp"

#include "Utilities/ArrayUtil.hpp"
#include "Utilities/Menu.hpp"

int main()
{
    bool done = false;

    char myArray[6];
    int size = 6;
    ArrayUtil::GiveRandomValues( myArray, size, 'A', 'Z' );

    char catArray[3] = { 'C', 'A', 'T' };
    size = 3;

    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "Sorting Algorithm Examples" );

        cout << " Randomly generated array: ";
        ArrayUtil::Cout( myArray, 6 );
        cout << endl;

        int choice = Menu::ShowIntMenuWithPrompt( {
            "Bubble Sort",
            "Insertion Sort",
            "Selection Sort",
            "Quick Sort",
            "Merge Sort",
            "Shell Sort",
            "Quit" } );

        switch( choice )
        {
            case 1:     bubbleSort(myArray, size );           break;
            case 2:     insertionSort(myArray, size );        break;
            case 3:     selectionSort(myArray, size );        break;
            case 4:     quickSort(myArray, 0, size-1 );       break;
            case 5:     mergeSort(myArray, 0, size-1 );       break;
            case 6:     shellSort(myArray, size );            break;
            case 7:     done = true; break;
        }

        if ( choice != 7 )
        {
            cout << " Sorted array: ";
            ArrayUtil::Cout( myArray, 6 );
        }

        Menu::Pause();
    }
}
