#include "SortFunctions.hpp"

void SelectionSort( vector<DataEntry>& data, const string& onKey )
{
    int n = data.size();

    for ( int j = 0; j < n-1; j++ )
    {
        int iMin = j;
        for ( int i = j+1; i < n; i++ )
        {
            if ( data[i].fields[ onKey ] < data[iMin].fields[ onKey ] )
            {
                iMin = i;
            }

            if ( iMin != j )
            {
                DataEntry temp = data[j];
                data[j] = data[iMin];
                data[iMin] = temp;
            }
        }
    }
}
void InsertionSort(vector<DataEntry>& data, const string& onKey)
{
	int n = data.size();

	for (int unsorted = 1; unsorted < n; unsorted++)
	{
		DataEntry nextItem = data[unsorted];
		int loc = unsorted;

		while ((loc > 0) && (data[loc - 1].fields[onKey] > data[unsorted].fields[onKey]))
		{
			data[loc].fields[onKey] = data[loc].fields[onKey];
			loc--;
		}
		data[loc] = nextItem;
	}
}
void MergeSort(vector<DataEntry>& data, int first, int last, const string& onKey)
{
	if (first < last) 
	{ 
		int mid = first + (last - first) / 2;
		MergeSort(data, first , mid, onKey);
		MergeSort(data, mid + 1, last, onKey);
		merge(data, first, mid, last, onKey);
	}
}
void merge(vector<DataEntry>& data, int first, int mid, int last, const string& onKey)
{
	int size = data.size;
    int first1 = first, last1 = mid, first2 = mid + 1, last2 = last;
	int index = first1;
	
	DataEntry temp = data[size];

	while ((first1 <= last1) && (first2 <= last2)) 
	{ 
		if (data[first1].fields[onKey] <= data[first2].fields[onKey]) 
		{ 
			temp.fields[onKey] = data[first1].fields[onKey];
			first1++; 
		} 
		else 
		{ 
			temp.fields[onKey] = data[first2].fields[onKey]; 
			first2++; 
		}
		index++; 
	}
	
	while (first1 <= last1)
	{
		temp.fields[onKey] = data[first1].fields[onKey]; 
		first1++;
		index++;
	}
	
	while (first2 <= last2)
	{
		temp.fields[onKey] = data[first2].fields[onKey]; 
		first2++; 
		index++;
	}
	
	for (index = first; index <= last; index++)
	{
		data[index].fields[onKey] = temp.fields[onKey];
	}
}
