#ifndef _SORT_FUNCTIONS_HPP
#define _SORT_FUNCTIONS_HPP

#include <iostream>
#include <vector>
using namespace std;

#include "DataEntry.hpp"

void SelectionSort( vector<DataEntry>& data, const string& onKey );
void InsertionSort(vector<DataEntry>& data, const string& onKey);
void MergeSort(vector<DataEntry>& data, int first, int last, const string& onKey);
void merge(vector<DataEntry>& data, int first, int mid, int last, const string& onKey);
#endif
