#ifndef DICTIONARY_HPP
#define DICTIONARY_HPP

#include <string>
#include <iostream>
using namespace std;

#include "DictionaryNode.hpp"
#include "../UTILITIES/Logger.hpp"
#include "../EXCEPTIONS/NotImplementedException.hpp"

enum CollisionMethod { LINEAR, QUADRATIC, DOUBLE };

template <typename TK, typename TV>
class Dictionary
{
    public:
    //! Initialize the dictionary
    Dictionary();
    //! Clean up the dictionary
    ~Dictionary();

    //! Set the dictionary's collision resolution method
    void SetCollisionMethod( CollisionMethod cm );

    //! Returns whether key is already in dictionary
    bool Exists( TK key );
    //! Insert a new item into the dictionary
    void Insert( TK key, TV value );
    //! Search for an item by its key
    TV* Get( TK key );
    //! Remove an item by its key
    void Remove( TK key );
    //! Returns the amount of items stored in the dictionary
    int Size();

    //! Write all the data to a text file
    void WriteToFile( const string& filename );

    private:
    //! Hash function to change the key into an index
    int HashFunction( TK key );
    //! Linear Probe method of collision resolution
    int LinearProbe( TK originalIndex, int collisionCount );
    //! Quadratic Probe method of collision resolution
    int QuadraticProbe( TK originalIndex, int collisionCount );
    //! Double Hash method of collision resolution
    int HashFunction2( TK key );

    //! Uses the Hash Function and Collision Functions to find a Node WITH the key given THAT ISN'T IN USE.
    int FindUnusedNode( TK key );
    //! Uses the Hash Function and Collision Functions to find a Node WITH the key given THAT IS IN USE.
    int FindNodeWithKey( TK key );

    //Vector<DictionaryNode<TK,TV> > m_vector;
    DictionaryNode<TK,TV>* m_vector;
    //! The collision resolution method to use
    CollisionMethod m_collisionMethod;
    int m_arraySize;
    int m_itemCount;

    // Friends
    friend class Tester;
};

/**
    Initializes its internal vector to a size with a prime number.
*/
template <typename TK, typename TV>
Dictionary<TK,TV>::Dictionary()         // done
{
    Logger::OutHighlight( "Function begin", "Dictionary::Dictionary", 2 );

    m_arraySize = 2251;
    m_vector = new DictionaryNode<TK,TV>[ m_arraySize ];
    m_itemCount = 0;
    m_collisionMethod = LINEAR;
}


/**
    If the m_vector is not nullptr, free that memory.
*/
template <typename TK, typename TV>
Dictionary<TK,TV>::~Dictionary()        // done
{
    if ( m_vector != nullptr )
    {
        delete [] m_vector;
    }
}

template <typename TK, typename TV>
int Dictionary<TK,TV>::HashFunction( TK key )
{
	return key % m_arraySize;
}

template <typename TK, typename TV>
int Dictionary<TK,TV>::LinearProbe( TK originalIndex, int collisionCount )
{
	return originalIndex + collisionCount;
}

template <typename TK, typename TV>
int Dictionary<TK,TV>::QuadraticProbe( TK originalIndex, int collisionCount )
{
	return originalIndex + (collisionCount * collisionCount);
}

template <typename TK, typename TV>
int Dictionary<TK,TV>::HashFunction2( TK key )
{
	return 7 - (key % 7);
}

template <typename TK, typename TV>
int Dictionary<TK,TV>::FindUnusedNode( TK key )
{
	int index = HashFunction(key);
	int originalIndex = index;
	int collisionCount = 0;
	int stepSize = 0;

	while (m_vector[index].used == true)
	{
		collisionCount++;

		if (m_collisionMethod == LINEAR)
		{
			index = LinearProbe(originalIndex, collisionCount);
		}
		else if (m_collisionMethod == QUADRATIC)
		{
			index = QuadraticProbe(originalIndex, collisionCount);
		}
		else if (m_collisionMethod == DOUBLE)
		{
			stepSize = HashFunction2(originalIndex);
			index = originalIndex + (collisionCount * stepSize);
		}
		index = index % m_arraySize;
	}
	return index;
}

template <typename TK, typename TV>
int Dictionary<TK, TV>::FindNodeWithKey(TK key)
{
	for (int i = 0; i < m_arraySize; i++)
	{
		if (m_vector[i].key == key && m_vector[i].used == true)
		{
			return i;
		}
	}
	return -1;

}
	/*
	int index = HashFunction(key);
	int originalIndex = index;
	int collisionCount = 0;
	int stepSize = 0;

	if (m_vector[index].key != key)
	{
		while (m_vector[index].used == true && m_vector[index].key != key)
		{
			collisionCount++;

			if (m_collisionMethod == LINEAR)
			{
				int index = LinearProbe(originalIndex, collisionCount);
			}
			else if (m_collisionMethod == QUADRATIC)
			{
				int index = QuadraticProbe(originalIndex, collisionCount);
			}
			else if (m_collisionMethod == DOUBLE)
			{
				stepSize = HashFunction2(originalIndex);
				int index = originalIndex + (collisionCount * stepSize);
			}
			index = index % m_arraySize;

			if (m_vector[index].key == key)
			{
				return true;
			}
			if (m_vector[index].used == false)
			{
				return false;
			}
		}

	}
*/




template <typename TK, typename TV>
bool Dictionary<TK,TV>::Exists( TK key )
{
	int index = FindNodeWithKey(key);

	if (index > 0 && index < m_arraySize - 1)
	{
		return true;
	}
	else if (index == -1)
	{
		return false;
	}
	
}

template <typename TK, typename TV>
void Dictionary<TK,TV>::Insert( TK key, TV value )
{
	if (Exists(key) == true)
	{
		// ignore
	}
	else
	{
		int index = FindUnusedNode(key);
		m_vector[index].key = key;
		m_vector[index].value = value;
		m_vector[index].used = true;
		m_itemCount++;
	}
}

template <typename TK, typename TV>
TV* Dictionary<TK,TV>::Get( TK key )
{
	int index = FindNodeWithKey(key);

	if (m_vector[index].key == key)
	{
		return &m_vector[index].value;
	}
	else
	{
		return nullptr;
	}
	
}
/* Already implemented functions */

/**
    @return int The amount of items stored in the dictionary currently.
*/
template <typename TK, typename TV>
int Dictionary<TK,TV>::Size()
{
    return m_itemCount;
}

/**
    Set the COLLISION METHOD that will be used should a collision occur.

    @param CollisionMethod cm       LINEAR, QUADRATIC, or DOUBLE.
*/
template <typename TK, typename TV>
void Dictionary<TK,TV>::SetCollisionMethod( CollisionMethod cm )
{
    m_collisionMethod = cm;
}

/* DONE */
template <typename TK, typename TV>
void Dictionary<TK,TV>::WriteToFile( const string& filename )
{
    Logger::OutHighlight( "Function begin", "Dictionary::WriteToFile", 2 );

    cout << "\t Output table to: " << filename << endl << endl;

    ofstream output( filename );
    output << "Index,Key,Value\n";
    for ( int i = 0; i < m_arraySize; i++ )
    {
        output << i << ",";

        if ( m_vector[i].used == true )
        {
            output << m_vector[i].key << "," << m_vector[i].value;
        }

        output << "\n";
    }
    output.close();
}

#endif